import java.util.Scanner;

public class Study_Itus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the student name: ");
		String studentName = keyboard.nextLine();

		System.out.println(studying(studentName));

	}

	public static String studying(String names) {

		String message;
		char comma = ',';
		int commaPosition = 0;

		if (names.isEmpty()) {
			message = "Nobody is studying";
		} else {
			char[] charArray = names.toCharArray();
			for (int i = 0; i < charArray.length; i++) {
				commaPosition++;
				if (charArray[i] == comma) {
					break;
				}
			}
			if (commaPosition >= charArray.length) {
				message = names + " is studying";
			} else
				message = names.substring(0, commaPosition - 1) + " and " + names.substring(commaPosition)
						+ " are studying";
		}

		return message.toUpperCase();
	}

}
